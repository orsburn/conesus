<?php

class LogFile
{
	public $hash;
	public $status;
	public $score;
	public $file;
	public $previousFile;  // If the file was renamed.
}