<?php

class Conesus
{
	public $repository;
	public $minHashLength = 4;  // We can't be reasonably sure that we can match a hash unless we look at, at least the first 4 characters.


	public function __construct($repository = null)
	{
		$this->repository = $repository;
	}


	/**
	 * Collects the files at a certain commit.
	 *
	 * @param string $commitId
	 * @return array Returns an array of LogFile objects.
	 */
	public function filesAtCommit($commitId)
	{
		if(func_num_args() == 2)
		{
			$logFiles = func_get_arg(1);
		}

		return array_values(array_filter($logFiles ?? $this->logFiles(), function($entry) use ($commitId){
			if(
				($entry->hash == $commitId) ||
				(substr($entry->hash, 0, max(strlen($commitId), $this->minHashLength)) == $commitId))
			{
					return true;
			}
		}));
	}


	/**
	 * Given a commit ID, this will return the files since, and including, that commit.
	 *
	 * @param string $commitId
	 * @return array Returns an array of LogFile objects.
	 */
	public function filesSinceCommit($commitId)
	{
		$listing = [];

		if(strlen($commitId) < $this->minHashLength)
		{
			$this->handleWarning("commitIdTooShort", $commitId);
		}

		$logFiles = $this->logFiles();

		foreach($this->commits() as $entry)
		{
			$listing = array_merge($listing, $this->filesAtCommit($entry, $logFiles));

			if(substr($entry, 0, strlen($commitId)) == $commitId)
			{
				break;
			}
		}

		return $listing;
	}


	public function filesBetweenCommits($startingCommitId, $endingCommitId)
	{
		$commits = $this->commits();
		$start = array_search($startingCommitId, $commits);
		$end = array_search($endingCommitId, $commits);
		$listing = [];

		if(!($start || $end))
		{
			return [];
		}

		foreach(array_slice($commits, $end, ($start - $end) + 1) as $entry)
		{
			$listing = array_merge($listing, $this->filesAtCommit($entry));
		}

		return $listing;
	}


	public function filesAtTag($tag)
	{
		return $this->filesAtCommit($this->commitForTag($tag));
	}


	public function filesSinceTag($tag)
	{
		return $this->filesSinceCommit($this->commitForTag($tag));
	}


	public function filesBetweenTags($startingTag, $endingTag)
	{
		return $this->filesBetweenCommits($this->commitForTag($startingTag), $this->commitForTag($endingTag));
	}


	/**
	 * Fetches the history for the repository.
	 *
	 * @param boolean $raw The function can also return the raw output from the git log command.  This will be returned as an array of entries, one commit per element.
	 * @return array Returns an array of objects.
	 */
	public function log()
	{
		if(!file_exists($this->repository))
		{
			$this->handleWarning("missingRepo", $this->repository);
		}

		chdir($this->repository);

		exec("git log --all --decorate --oneline", $log);

		return array_map(function($element){
			$splitHere = strpos($element, " ");

			$output = new LogEntry();

			$output->hash = substr($element, 0, $splitHere);
			$output->message = substr($element, $splitHere + 1);  // Plus 1 to skip the space we split on.

			return $output;
		}, $log);
	}


	public function rawLog()
	{
		if(!file_exists($this->repository))
		{
			$this->handleWarning("missingRepo", $this->repository);
		}

		chdir($this->repository);

		exec("git log --all --decorate --oneline", $log);

		return $log;
	}


	/**
	 * Gets all the files that were touched in a commit for the entire repository history.
	 *
	 * @return array Returns an array of LogFile objects.
	 */
	public function logFiles()
	{
		chdir($this->repository);
		exec("git log --all --name-status --oneline --pretty=format:\"%H\"", $log);

		$results = [];
		$hash = null;

		foreach($log as $entry)
		{
			$logFile = new LogFile();
			$fields = explode("\t", $entry);

			if(count($fields) >= 2)
			{
				$logFile->hash = $hash;
				$logFile->status = $fields[0];

				if(($fields[0]{0} === 'R') || ($fields[0]{0} === 'C'))
				{
					$logFile->status = $fields[0]{0};
					$logFile->score = (integer) substr($fields[0], 1);
				}

				if(count($fields) === 3)
				{
					$logFile->file = $fields[2];
					$logFile->previousFile = $fields[1];  // If the file was renamed.
				}
				else
				{
					$logFile->file = $fields[1];
				}

				$results[] = $logFile;
			}
			elseif($fields[0] !== '')
			{
				$hash = $fields[0];
			}
		}

		return $results;
	}


	public function commits()
	{
		chdir($this->repository);
		exec("git log --all --decorate --oneline --pretty=format:\"%H\"", $log);

		return $log;
	}


	public function commitForTag($tag)
	{
		/*
		This method works the way it does because there no good way to handle the case
		where the tag being sought doesn't exist.  Any such checks would likely require
		multiple calls to git on the system.  It's probably faster to just parse some
		git output.

		Examples that have been tried:

			git tag -v [TAG]
			git rev-parse [TAG]
			git rev-list -n [TAG]
		*/
		$outputCommitId = null;

		chdir($this->repository);
		exec("git show-ref --tags --dereference", $listing);

		foreach($listing as $entry)
		{
			list($commitId, $tagRef) = explode(" ", $entry);

			$tagCheck = substr($tagRef, strrpos($tagRef, "/") + 1);

			/*
			The entire list is scanned rather than tring to be clever about accommodating
			annotated tags.  Based on the listing from git log, it looks like the hash that
			should be output is the one with the annotation if one exists.
			*/
			if(($tagCheck === $tag) || ($tagCheck === "$tag^{}"))  // BUG  This will likely need to be updated to allow content betweeb the curly braces.
			{
				$outputCommitId = $commitId;
			}
		}

		return $outputCommitId;
	}


	private function handleWarning($warning, $details = null)
	{
		switch($warning)
		{
			case "missingRepo": echo "The repository ($details) could not be found.\n";
				throw new Exception("The repository could not be found." . PHP_EOL);
			case "commitIdTooShort":
				throw new Exception("The commit ID ($details) is too short to reliably match against commit hashes." . PHP_EOL);
		}
	}
}