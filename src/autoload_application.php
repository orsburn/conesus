<?php

/* =============================================================================
The loader works by living at the application's root, and appending the
namespace to it.  This is where the definition will be sought.

    NOTE:  This will /only/ look in paths for your application and below with
           the added feature described below.

Example
-------

	$sample = new lib\TargetClass();

		Maps to application path:

	/path/to/exampleApplication/lib/TargetClass.php

This also tries to incorporate the Composer autoloader as well, so there's no
need to explicitly include the Composer autoload file.  That is, assuming that
the vendor directory is either up one level from the source directory, or is in
the source directory itself.

	The check order:

		1.  /application/vendor
		2.  /application/src/vendor


Bonus
-----

As a bonus, it will also look over an array of other library paths indicated in
a global variable $autoloadLibs.  Add paths to libraries up to the point that
matches a namespace path.

For example:

	$autoloadLibs[] = "/libApplication/src";

This would allow the following files to be autoloaded.

	/libApplication/src/models/Horse.php    as    new models\Horse()
	/libApplication/src/lib/Car.php         as    new lib\Car()
============================================================================= */

if(file_exists(__DIR__ . "/../vendor/autoload.php"))   // | Composer packages are sought first so that
{                                                      // | they may be able to used in application
	include_once __DIR__ . "/../vendor/autoload.php";  // | classes too.
}                                                      // |
													   // | Looking for vendor up one level first, then
if(file_exists(__DIR__ . "/vendor/autoload.php"))      // | trying to find it in the application's own
{                                                      // | directory.
	include_once __DIR__ . "/vendor/autoload.php";     // |
}                                                      // | For example:
													   // |
													   // |     1.  /application/vendor
													   // |     2.  /application/src/vendor

\spl_autoload_register("applicationAutoload");


function applicationAutoload($target)
{
	global $autoloadLibs;

	// print_r(str_replace("\\", "/", __DIR__ . "/$target.php"));                                                                     // | Uncomment these lines to verify that
	// print(" [" . (file_exists(str_replace("\\", "/", __DIR__ . "/$target.php")) ? "Exists!  Yay!" : "Missing.  Wah, wah.") . "]");  // | your class is being picked up.

	if(file_exists(str_replace("\\", "/", __DIR__ . "/$target.php")))
	{
		include_once str_replace("\\", "/", __DIR__ . "/$target.php");

		return;
	}

	foreach($autoloadLibs ?? [] as $path)
	{
		if(file_exists(str_replace("\\", "/", "$path/$target.php")))
		{
			include_once str_replace("\\", "/", "$path/$target.php");
		}
	}
}