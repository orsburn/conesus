*The actual readme is found in the **README** folder.  The more consistent nature of HTML documents over Markdown documents was favored for an elaborate readme file.*

# Origin

The idea for this project was originally hatched from an idea for creating scripts to automate the deployment of applications directly from Git repositories.  There is no real API support for Git, so a PHP wrapper around was Git was born.

# Example

The **/example.php** file can be experimented with via the command line to see the types of output that's generated.