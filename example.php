<?php

include_once "src/autoload_application.php";


$con = new Conesus("/path/to/repository");

// -----------------------------------------------------------------------------
// print_r($con->filesAtCommit("COMMIT_HASH"));

// -----------------------------------------------------------------------------
// print_r($con->filesSinceCommit("COMMIT_HASH"));

// -----------------------------------------------------------------------------
// print_r($con->filesBetweenCommits("OLDEST_COMMIT_HASH", "NEWEST_COMMIT_HASH"));

// -----------------------------------------------------------------------------
// print_r($con->filesAtTag("TAG"));

// -----------------------------------------------------------------------------
// print_r($con->filesSinceTag("TAG"));

// -----------------------------------------------------------------------------
// print_r($con->filesBetweenTags("OLDEST_TAG", "NEWEST_TAG"));

// -----------------------------------------------------------------------------
// print_r($con->log());

// -----------------------------------------------------------------------------
// print_r($con->rawLog());

// -----------------------------------------------------------------------------
// print_r($con->logFiles());

$output = $con->logFiles();

print_r("         Hash:  {$output[2]->hash}\n");
print_r("       Status:  {$output[2]->status}\n");
print_r("        Score:  {$output[2]->score}\n");
print_r("         File:  {$output[2]->file}\n");
print_r("Previous file:  {$output[2]->previousFile}\n");

// -----------------------------------------------------------------------------
// print_r($con->commits());

// -----------------------------------------------------------------------------
// print_r($con->commitForTag("1.6.22"));